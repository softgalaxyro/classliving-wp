Version 1.0
	Release

Version 1.1
	Added: Built-in demo data import

Version 1.2
	Fixed bugs in the theme's files
	
Version 1.3
	Search form fixed
	Visual Composer updated

Version 1.4	
	Visual Composer updated
	