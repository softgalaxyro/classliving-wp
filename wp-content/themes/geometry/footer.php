<?php
/**
 * The template for displaying the footer.
 * @package Geometry
 */

global $logo_footer; 

				$body_style =  get_custom_option('body_style');
				$side_bar = get_custom_option('show_sidebar_main');
				$fstyle = strpos(get_custom_option('blog_style'),'portfolio') !== false;

				echo ($body_style == 'boxed' &&  $side_bar != 'fullWidth' && !$fstyle) ? '</div><!-- /.main -->' : '' ?>
			</div><!-- /.wrapContent > /.content -->
			<?php get_sidebar(); //sidebar ?>
		</div><!-- /.wrapContent > /.wrapWide -->
	</div><!-- /.wrapContent -->

	<?php 
	// ----------------- Google map -----------------------
	if ( get_custom_option('googlemap_show') == 'yes' ) { 
		$map_address = get_custom_option('googlemap_address');
		$map_latlng = get_custom_option('googlemap_latlng');
		$map_zoom = get_custom_option('googlemap_zoom');
		$map_scroll = get_custom_option('googlemap_scroll');
		$map_style = get_custom_option('googlemap_style');
		if (!empty($map_address) || !empty($map_latlng)) { 

			echo do_shortcode('[trx_googlemap id="footer" latlng="'.$map_latlng.'" address="'.$map_address.'" zoom="'.$map_zoom.'" scroll="'.$map_scroll.'" style="'.$map_style.'" width="100%" height="350"]');
		
		} 
	}

	// -------------- footer -------------- 
	$footer_widget = (get_custom_option('show_sidebar_footer') == 'yes' && is_active_sidebar( get_custom_option('sidebar_footer')));
	if( $footer_widget){
		$footer_columns = get_custom_option('widget_columns_footer'); 
	?>  
		<footer <?php echo balanceTags($footer_widget ? 'class="footerWidget"' : '') ?>>
			<div class="main">
				<div class="sc_columns_indent sc_columns_<?php echo balanceTags($footer_columns); ?>">
				<?php  // ---------------- Footer sidebar ----------------------
				if ( $footer_widget  ) { 
					global $THEMEREX_CURRENT_SIDEBAR;
					$THEMEREX_CURRENT_SIDEBAR = 'footer'; 
						do_action( 'before_sidebar' );
						if ( !dynamic_sidebar( get_custom_option('sidebar_footer') ) ) {
							// Put here html if user no set widgets in sidebar
						}
				} 
				 ?>
				 </div>
			</div><!-- /footer.main -->
		</footer>
		<?php
	} ?>
</div><!-- /.wrapBox -->


	<?php
	// -------------- footer -------------- 
	$custom = sc_param_is_on(get_custom_option('show_copyright'));
	if($custom)
	{   
		$socials = get_theme_option('social_icons');
		$icons = '';
		foreach ($socials as $s) {
			if (empty($s['url'])) continue;
			
				preg_match("/(\/)[a-z]+(.png)/", $s['icon'], $a);
				preg_match("/[a-z]+/", $a[0], $b);
				$icons = $icons
						.'<li><a class="social_icons icon-'
						.$b[0].'" href="'
						.$s['url'].'" target="_blank"></a></li>';
				
		} 

		echo '<div class="custom_footer">'
			 .'<ul class="custom_socials">'
			 .$icons
			 .'</ul>'
			 .'</div>';
		
	}
	
	
	$copyright = sc_param_is_on(get_custom_option('show_copyright'));
	if($copyright){
		$copy_footer = get_theme_option('footer_copyright');
		if ( $copy_footer != '' && $copyright ){
			?><div class="copyright">
				<div class="main">
					<?php
					print str_replace('[year]',date('Y'), $copy_footer);
					?>
					<!-- <a href="" class="visa_logo">
						<img src="http://geometry.themerex.net/wp-content/themes/Geometry/images/Visa_Logo.png" alt="">
					</a> -->
				</div>
			</div><?php
		}
	} ?>
	
</div><!-- /.wrap -->


<div class="buttonScrollUp upToScroll icon-up-open-micro"></div>

<?php 
require(get_template_directory() . '/templates/page-part-login.php');
require(get_template_directory() . '/templates/page-part-js-messages.php');
require(get_template_directory() . '/templates/page-part-customizer.php');
wp_footer(); 
?>
</body>
</html>
