<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $product, $woocommerce_loop;

// Store loop count we're currently on
if (empty($woocommerce_loop['loop'])) {
    $woocommerce_loop['loop'] = 0;
    $delay_count = 1;
}

// Increase loop count
$woocommerce_loop['loop'] ++;
$delay_count = 1 + $woocommerce_loop['loop'] * 0.3;

// Extra post classes
$classes = array('class-li', 'extra', '');
?>
<li <?php post_class($classes); ?> style="animation-delay: <?php echo $delay_count; ?>s;">

    <?php do_action('woocommerce_before_shop_loop_item'); ?>

    <a href="<?php the_permalink(); ?>">

        <?php $badge = $product->get_attribute('pa_insigna');
        if ($badge) :
            ?> 
            <span class="onsale"><?php echo $badge; ?></span> 
        <?php endif; ?>

<?php do_action('woocommerce_before_shop_loop_item_title'); ?>

        <div class="overlay-effect-3 ">
            <div class="overlay-effect">
                <div class="box-titles">
                    <h3 itemprop="name" class="product_title entry-title"><?php $title = apply_filters('the_title', get_the_title( $product->id), $product->id);
 echo $title; ?></h3>
                    <?php $subtitle = get_the_terms(get_the_ID(), 'pa_sub-titlu');
                    if ($subtitle) :
                        ?> 
                        <h4 itemprop="name" class="product_subtitle entry-title"><?php echo $subtitle[0]->name; ?></h4> 
<?php endif; ?> 
                </div>
            </div>
        </div>

<?php do_action('woocommerce_after_shop_loop_item_title'); ?>

    </a>
    <a class="button" rel="nofollow" href="<?php echo get_permalink($product->id); ?>" title="<?php echo do_shortcode('[text_module slug="read-more"]'); ?>"><?php echo do_shortcode('[text_module slug="read-more"]'); ?></a>

<?php do_action('woocommerce_after_shop_loop_item'); ?>

</li>
