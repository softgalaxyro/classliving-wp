<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$is_ajax = $_GET['ajax'] == '1' ? true : false;

if (!$is_ajax) {
    get_header('shop');

    /**
     * woocommerce_before_main_content hook
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     */
    do_action('woocommerce_before_main_content');

    if (apply_filters('woocommerce_show_page_title', true)) {
        ?>
        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
        <?php
    }

    /**
     * woocommerce_archive_description hook
     *
     * @hooked woocommerce_taxonomy_archive_description - 10
     * @hooked woocommerce_product_archive_description - 10
     */
    do_action('woocommerce_archive_description');
}

if (have_posts()) {
    /**
     * woocommerce_before_shop_loop hook
     *
     * @hooked woocommerce_result_count - 20
     * @hooked woocommerce_catalog_ordering - 30
     */
    //do_action( 'woocommerce_before_shop_loop' );

    if (!$is_ajax) {
        //woocommerce_product_loop_start();
        echo '<ul class="products ajax-list">';
    }

    woocommerce_product_collections_new(); //defined in template function file

    $array_categories = woocommerce_product_collections_new();

    //woocommerce_product_subcategories();

    $products_array = array();

    while (have_posts()) : the_post();
        global $product;
        $product->ptype = 'prod';
        $products_array[] = $product;
    endwhile;
    wp_reset_postdata();

    $has_search = false;
    if (isset($_GET['s'])) {
        $has_search = true;
        $searchword = strtolower($_GET['s']);
        $_SESSION['has_search'] = $has_search;
        $_SESSION['searchword'] = $searchword;
    }

    if ($has_search) {
        while (have_posts()) : the_post();

            $product = wc_get_product($product);
            wc_get_template('content-product_new.php', array('product' => $product->post->ID));

        endwhile; // end of the loop. 
    } else {
        //$loop_array = $array_categories;
        $loop_array = array_merge($array_categories, $products_array);

        // Display blocks   
        $queried_object = get_queried_object();

        // get current page
        $page = $_GET['pag'] ? : 1;

        if ($page == 1) {
            $limit = 9;
            $offset = 0;
        } else {
            $limit = 3;
            $offset = 9 + ($page - 2) * $limit;
        }

        $subloop_array = array_slice($loop_array, $offset, $limit);

        $has_cat = false;
        foreach ($subloop_array as $item) {
            if (in_array($item->ptype, array('cat', 'subcat'))) {
                $has_cat = true;
            }
        }

        foreach ($subloop_array as $loop_element) {
            if ($loop_element->ptype == 'cat') {
                wc_get_template('content-product_cat.php', array('category' => $loop_element));
            } elseif ($loop_element->ptype == 'subcat') {
                wc_get_template('content-product_subcat.php', array('category' => $loop_element));
            } elseif ($loop_element->ptype == 'prod' && $has_cat == false) {
                $product = $loop_element;
                wc_get_template('content-product_new.php', array('product' => $loop_element->post->ID));
            }
        }
    }



    if (!$is_ajax) {
        woocommerce_product_loop_end();
    }

    /**
     * woocommerce_after_shop_loop hook
     *
     * @hooked woocommerce_pagination - 10
     */
    //do_action( 'woocommerce_after_shop_loop' );
} elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) {

    wc_get_template('loop/no-products-found.php');
}

if (!$is_ajax) {
    /**
     * woocommerce_after_main_content hook
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action('woocommerce_after_main_content');



    /**
     * woocommerce_sidebar hook
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    do_action('woocommerce_sidebar');

    get_footer('shop');
}