<?php
/**
 * The template for displaying product category thumbnails within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.0
 */
if (!defined('ABSPATH')) {
    exit;
}

global $woocommerce_loop;

// Store loop count we're currently on
if (empty($woocommerce_loop['loop'])) {
    $woocommerce_loop['loop'] = 0;
    $delay_count = 1;
}

// Store column count for displaying the grid
if (empty($woocommerce_loop['columns'])) {
    $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);
}

// Increase loop count
$woocommerce_loop['loop'] ++;
$delay_count = 1 + $woocommerce_loop['loop'] * 0.3;
?>
<li <?php wc_product_cat_class(); ?> style="animation-delay: <?php echo $delay_count; ?>s;"  rel="custom-subcat">
    
    <?php do_action('woocommerce_before_subcategory', $category); ?>
    
    <a href="<?php echo get_term_link($category->slug, 'product_cat'); ?>">
        
        <?php do_action('woocommerce_before_subcategory_title', $category); ?>

        
            <div class="isotopeItemWrap">
                <div class="isotopeRating" title="<?php echo $category->count; ?>">
                    <span class="rInfo"><?php echo $category->count; ?></span>
                    <span class="rDelta">
                        <span class="icon-star"></span>
                    </span>
                </div>
            </div>
        

        <div class="overlay-effect-2 ">
            <div class="overlay-effect">
                 <h3><?php echo $category->name; ?></h3>
            </div>
           
        </div>

        <?php do_action('woocommerce_after_subcategory_title', $category); ?>
        
    </a>
    
    <?php do_action('woocommerce_after_subcategory', $category); ?>
    
</li>
