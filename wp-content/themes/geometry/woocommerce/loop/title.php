<?php
/**
 * Product loop title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<h3 itemprop="name" class="product_subtitle entry-title"><?php the_title(); ?></h3>
<?php $subtitle = get_the_terms(get_the_ID(), 'pa_sub-titlu');
if ($subtitle) : 
    $subtitle = apply_filters('the_title', $subtitle[0]->name, get_the_ID()); ?> 
    <h4 itemprop="name" class="product_subtitle entry-title"><?php echo $subtitle[0]->name; ?></h4> 
<?php endif; ?> 
