<?php
/**
 * Single Product title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>
<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>

<?php
$subtitle = get_the_terms(get_the_ID(), 'pa_sub-titlu');
if ($subtitle) :
    $subtitle = apply_filters('the_title', $subtitle[0]->name, get_the_ID());
    ?> 
    <h2 itemprop="name" class="product_subtitle entry-title"><?php echo $subtitle; ?></h2> 
<?php endif; ?> 