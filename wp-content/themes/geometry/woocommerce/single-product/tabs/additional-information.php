<?php
/**
 * Additional Information tab
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Additional Information', 'woocommerce' ) );

?>

<?php if ( $heading ): ?>
	<h2><?php echo $heading; ?></h2>
<?php endif; ?>

<?php $product->list_attributes(); ?>

        
        
<?php

/**
global $woocommerce, $product, $post;
  $args = array(
   'post_type' => 'attachment',
   'orderby' => 'menu_order',
   'numberposts' => -1,
   'post_status' => null,
   'post_parent' => $post->ID,
   'post_mime_type' => array( 'application/pdf','application/vnd.ms-excel','application/msword' )
  );

  $attachments = get_posts( $args );

     if ( $attachments ) {

        foreach ( $attachments as $attachment ) {
             
        // SETUP THE ATTACHMENT ICON
		$attachment_icon = $attachment->post_mime_type;
		$attachment_icon = explode( '/',$attachment_icon ); 
		$attachment_icon = $attachment_icon[1];
		$attachment_icon = '<img src="' .get_bloginfo('template_directory'). '/icons/' . $attachment_icon . '.png" alt="' . get_the_title($attachment->ID) . '" title="' . get_the_title($attachment->ID) . '" />';

           echo '<p><a href="';
           echo wp_get_attachment_url( $attachment->ID );
           echo '">';
           echo $attachment_icon;
           echo wp_get_attachment_image( $attachment->ID );
           echo '&nbsp;&nbsp;';
           echo apply_filters( 'the_title', $attachment->post_title );
           echo '</a>';
           echo '</p>';
          }
     }
 * 
 * */
?>