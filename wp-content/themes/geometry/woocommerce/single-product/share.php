<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_share' ); // Sharing plugins can hook into here 
	$facebook_icon = '<img src="' .get_bloginfo('template_directory'). '/icons/' . "facebook" . '.png" alt="' . get_the_title($attachment->ID) . '" title="' . get_the_title($attachment->ID) . '" />';
        $twitter_icon = '<img src="' .get_bloginfo('template_directory'). '/icons/' . "twitter" . '.png" alt="' . get_the_title($attachment->ID) . '" title="' . get_the_title($attachment->ID) . '" />';
        $googleplus_icon = '<img src="' .get_bloginfo('template_directory'). '/icons/' . "googleplus" . '.png" alt="' . get_the_title($attachment->ID) . '" title="' . get_the_title($attachment->ID) . '" />';
        $linkedin_icon = '<img src="' .get_bloginfo('template_directory'). '/icons/' . "linkedin" . '.png" alt="' . get_the_title($attachment->ID) . '" title="' . get_the_title($attachment->ID) . '" />';
        
	$product_link = "http://".$_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI];
        $product_title= get_the_title($attachment->ID);
	//$fbshare = "javascript:fbShare('" .$product_link . "', '', '', '', 520, 350)";
	//$link_on_image_share = '<p><a href="' . $fbshare . '">' . $facebook_icon . '</a>' . '</p>';
	//echo $link_on_image_share;
	?>

<p><?php echo do_shortcode( "[apss_share networks='facebook, twitter, google-plus, linkedin' counter='1']" ); ?></p>	

<!--	<script>
	 function fbShare(url, title, descr, image, winWidth, winHeight) {
	        var winTop = (screen.height / 2) - (winHeight / 2);
	        var winLeft = (screen.width / 2) - (winWidth / 2);
	        window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + 
	    	        '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + 
	    	        image, 'sharer', 'top=' + winTop + ',left=' + winLeft + 
	    	        ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	 }
	</script>-->
	
	

