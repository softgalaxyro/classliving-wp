<?php

$show_title = get_custom_option('show_post_title', null, $post_data['post_id'])=='yes' && (get_custom_option('show_post_title_on_quotes')=='yes' || !in_array($post_data['post_format'], array('aside', 'chat', 'status', 'link', 'quote')));


$item_style = explode('_', get_custom_option('portfolio_item_style',null,$post_data['post_id']));
$item_hover = get_custom_option('portfolio_item_hover',null,$post_data['post_id']);

$thumb_crop = array( 'portfolio_big' => '1','portfolio_medium' => '2','portfolio_mini' => '3');
$thumb_sizes = getThumbSizes(array(
	'thumb_size' => themerex_substr($post_data['post_layout'], 0, 7) == 'portfol' ? getThumbColumns('cub',$thumb_crop[$post_data['post_layout']]) : '',
	'thumb_crop' => true,
	'sidebar' => false
));
$thumb_sizes['w'] = $thumb_sizes['w'] * $item_style[1];
$thumb_sizes['h'] = $thumb_sizes['h'] * $item_style[2];
$thumb_img = getResizedImageURL($post_data['post_attachment'], $thumb_sizes['w'], $thumb_sizes['h']);



?>

	<article class="isotopeItem <?php 
		$itemBG = get_custom_option('cube_color',null,$post_data['post_id']); 
		echo 'post_format_'.$post_data['post_format'] 
			.(' isw_'.$item_style[1])
			.(' hoverStyle_'.$item_hover)
			.($opt['number']%2==0 ? ' even' : ' odd') 
			.($opt['number']==0 ? ' first' : '') 
			.($opt['number']==$opt['posts_on_page'] ? ' last' : '')
			.($opt['add_view_more'] ? ' viewmore' : '') 
			.($itemBG !='' && !$post_data['post_thumb'] != '' ? ' showBG' : '')
			.(get_custom_option('show_filters')=='yes' 
				? ' flt_'.join(' flt_', get_custom_option('filter_taxonomy')=='categories' ? $post_data['post_categories_ids'] : $post_data['post_tags_ids'])
				: '');
		?>" data-postid="<?php echo (int) $post_data['post_id'] ?>" data-wdh="<?php echo (int) $thumb_sizes['w'] ?>" data-hgt="<?php echo (int) $thumb_sizes['h'] ?>" data-incw="<?php echo esc_attr($item_style[1]) ?>" data-inch="<?php echo esc_attr($item_style[2]) ?>"
			<?php echo balanceTags($itemBG !='' && !$post_data['post_thumb'] != '' ? 'style="background-color: '.esc_attr($itemBG).';"' : '') ; ?>>
		<div class="isotopeItemWrap">
			
			
			<?php 
			
			$data_link = $post_data['post_link'];
			if( get_custom_option('post_single_link',null,$post_data['post_id']) != '') $data_link = get_custom_option('post_single_link',null,$post_data['post_id']);
			
			//thumb
			if ($post_data['post_thumb']) { ?>
			    <?php if($item_hover == 4) echo '<a href="'.$data_link.'" class="isotopeThumb">'; ?>
				<div class="thumb">
					<?php echo balanceTags($post_data['post_format']  == 'video' ? '<span class="cube_icon icon-play-line"></span>' : ''); ?>
				<!--	<img src="<?php echo esc_url($thumb_img); ?>" alt="<?php echo balanceTags($post_data['post_title']) ?>"> -->
					<div class="img" style='background-image: url("<?php echo esc_url($thumb_img); ?>");' ></div>
				</div>
				<?php
			   if($item_hover == 4) echo '</a>';
			} 

			//review
			if( $post_data['post_reviews_author'] ){
				$avg_author = $post_data['post_reviews_'.(get_theme_option('reviews_first')=='author' ? 'author' : 'users')];
				$rating_max = get_custom_option('reviews_max_level');
				$reviews_style = get_custom_option('reviews_style'); 
				$review_title = sprintf($rating_max<100 ? __('Rating: %s from %s', 'themerex') : __('Rating: %s', 'themerex'), number_format($avg_author,1).($rating_max < 100 ? '' : '%'), $rating_max.($rating_max < 100 ? '' : '%'));?>

				<div class="isotopeRating" title="<?php echo balanceTags($review_title); ?>"><span class="rInfo"><?php echo balanceTags($avg_author); ?></span><span class="rDelta"><?php echo balanceTags($rating_max < 100 ? '<span class="icon-star"></span>' : '%'); ?></span></div>
			<?php } 
			
			if( $post_data['post_thumb'] ){ ?>
				<div class="isotopeContentWrap">
					<div class="isotopeContent">
						<h4 class="isotopeTitle">
							<?php $showsingleLink = sc_param_is_on(get_custom_option('show_single_link',null,$post_data['post_id'])); 
								echo balanceTags($showsingleLink && $item_hover != 3 ? '<a href="'.esc_url($data_link).'">' : '');
								echo getShortString($post_data['post_title'],35);
								echo balanceTags($showsingleLink && $item_hover != 3 ? '</a>' : '');
							?>
						</h4>
						<?php echo balanceTags($item_hover == 3 ? '<div class="show_more"><a href="'.esc_url($data_link).'">show more</a></div>' : ''); ?>
						<?php echo balanceTags($post_data['post_excerpt'] ? '<div class="isotopeExcerpt">'.getShortString(($post_data['post_excerpt']), 70 ).'</div>' : ''); 
						//postinfo
						echo getPostInfo(get_custom_option('set_post_info',null,$post_data['post_id']),$post_data,false); 
						?>
					</div>
				</div>
			<?php } else { ?>
				<div class="isotopeStatickWrap">
					<div class="isotopeStatick">
						<div class="postFormatIcon <?php echo getPostFormatIcon($post_data['post_format']) ?>"></div>
						<?php if ($post_data['post_format']  != 'link') { ?>
						<h5 class="isotopeTitle">
							<?php $showsingleLink = sc_param_is_on(get_custom_option('show_single_link',null,$post_data['post_id'])); 
								echo balanceTags($showsingleLink ? '<a href="'.esc_url($data_link).'">' : '');
								echo getShortString($post_data['post_title'],35);
								echo balanceTags($showsingleLink ? '</a>' : '');
							?>
						</h5>
						<?php } else {
							echo '<a class="isotopeLinks" href="'.$post_data['post_title'].'">'.getShortString($post_data['post_title'],35).'</a>';
						}
						//postinfo
						echo balanceTags($post_data['post_format']  != 'link'  ? getPostInfo(get_custom_option('set_post_info',null,$post_data['post_id']),$post_data,false) : ''); 
						$post_excerpt = getShortString(strip_tags($post_data['post_excerpt']), 150 );
						if($item_hover == 4) $post_excerpt = $post_data['post_excerpt'];
						echo balanceTags($post_data['post_excerpt'] && $post_data['post_format']  != 'quote' && $post_data['post_format']  != 'link' ? '<div class="isotopeExcerpt">'.$post_excerpt .'</div>' : ''); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</article>