// global jQuery:false 
var THEMEREX_ADMIN_MODE = false;
var THEMEREX_error_msg_box = null;
var THEMEREX_VIEWMORE_BUSY = false;
var THEMEREX_REMEMBERSCROLL = 0;
var THEMEREX_isotopeInitCounter = 0;
var THEMEREX_isotopeMemoryID = '';
var THEMEREX_isotopeFilter = '*';


jQuery(document).ready(function() {
    
    jQuery('select option').hover(
        function() {
            jQuery(this).addClass('highlight');
        }, function() {
            jQuery(this).removeClass('highlight');
        }
    );
    
    "use strict";
    if (document.documentElement.clientWidth < 700) {
	jQuery("#brand-div").height(jQuery(".newsletter-subscription").height());
        jQuery("#colectii-div").height(jQuery(".newsletter-subscription").height());
}
    //jQuery("#woof_results_by_ajax ul").addClass("animated fadeInUp");
    timelineResponsive()
    ready();
    itemPageFull();
    scrollAction();
    fullSlider();
        var delayTrans = 1;
    jQuery(".class-li").each(function(){
        this.style.animationDelay = delayTrans + "s";
        console.log(delayTrans +"s");
        delayTrans = delayTrans +0.3;
    });
    
    
    
});
 
     



jQuery(window).resize(function() {
    "use strict";
    itemPageFull();
    timelineResponsive();
    fullSlider();
    scrollAction();
});

jQuery(window).smartresize(function() {
    mobileMenuShow();
});

jQuery(window).scroll(function() {
    "use strict";
    scrollAction();
});

jQuery(window).load(function() {
    jQuery('.sc_blogger.style_image .sc_scroll.sc_scroll_horizontal .sc_scroll_wrapper').css('display', 'flex');

    jQuery('.sc_blogger.style_image .thumb').each(function() {
        var x = jQuery(this).width();
        jQuery(this).height(x);

        var image_X = jQuery(this).find('img').width();
        var image_Y = jQuery(this).find('img').height();

        if (image_Y < image_X) jQuery(this).find('img').height('100%');
        else jQuery(this).find('img').width('100%');
    });

    jQuery('.woocommerce.woo_products_style_2 li.product').each(function(){
        var href = jQuery(this).find('> a').attr('href');
    	jQuery(this).find('.button.add_to_cart_button').html('read details');
        jQuery(this).find('.button.add_to_cart_button').attr('href', href);
        jQuery(this).find('.button.add_to_cart_button .added_to_cart').hide();
        jQuery(this).find('.button.add_to_cart_button').click(function(){
            window.location = href;
        });
        jQuery(this).find('.button.add_to_cart_button').removeClass('add_to_cart_button');

    });
});

function ready() {
    "use strict";

    //textarea Autosize
    if (jQuery('textarea.textAreaSize').length > 0) {
        jQuery('textarea.textAreaSize').autosize({
            append: "\n"
        });
    }

    // Share button
    if (jQuery('ul.shareDrop').length > 0) {
        jQuery(document).click(function(e) {
            "use strict";
            jQuery('ul.shareDrop').slideUp().siblings('a.shareDrop').removeClass('selected');
        });
        jQuery('li.share a').click(function(e) {
            "use strict";
            if (jQuery(this).hasClass('selected')) {
                jQuery(this).removeClass('selected').siblings('ul.shareDrop').slideUp();
            } else {
                jQuery(this).addClass('selected').siblings('ul.shareDrop').slideDown();
            }
            e.preventDefault();
            return false;
        });
        jQuery('li.share li a').click(function(e) {
            jQuery(this).parents('ul.shareDrop').slideUp().siblings('a.shareDrop').removeClass('selected');
            e.preventDefault();
            return false;
        });
    }

    // Like button
    jQuery('.postSharing,.masonryMore').on('click', '.likeButton a', function(e) {
        var button = jQuery(this).parent();
        var inc = button.hasClass('like') ? 1 : -1;
        var post_id = button.data('postid');
        var likes = Number(button.data('likes')) + inc;
        var grecko_likes = jQuery.cookie('grecko_likes');
        if (grecko_likes === undefined) grecko_likes = '';
        jQuery.post(THEMEREX_ajax_url, {
            action: 'post_counter',
            nonce: THEMEREX_ajax_nonce,
            post_id: post_id,
            likes: likes
        }).done(function(response) {
            var rez = JSON.parse(response);
            if (rez.error === '') {
                if (inc == 1) {
                    var title = button.data('title-dislike');
                    button.removeClass('like').addClass('likeActive');
                    grecko_likes += (grecko_likes.substr(-1) != ',' ? ',' : '') + post_id + ',';
                } else {
                    var title = button.data('title-like');
                    button.removeClass('likeActive').addClass('like');
                    grecko_likes = grecko_likes.replace(',' + post_id + ',', ',');
                }
                button.data('likes', likes).find('a').attr('title', title).find('.likePost').html(likes);
                jQuery.cookie('grecko_likes', grecko_likes, {
                    expires: 365,
                    path: '/'
                });
            } else {
                themerex_message_warning(THEMEREX_MESSAGE_ERROR_LIKE);
            }
        });
        e.preventDefault();
        return false;
    });

    //hoverZoom img effect
    jQuery('.hoverIncrease').each(function() {
        "use strict";
        var img = jQuery(this).data('image');
        var title = jQuery(this).data('title');
        if (img) {
            jQuery(this).append('<span class="hoverShadow"></span><a href="' + img + '" title="' + title + '"><span class="hoverIcon"></span></a>');
        }
    });

    // ====== isotope =====================================================================
    if (jQuery('.isotopeWrap').length > 0) {

        jQuery('.isotopeWrap').each(function() {
            isotopeFilterClass('*');

            var isotopeWrap = jQuery(this);
            var isotopeItem = isotopeWrap.find('.isotopeItem');
            var isotopeWrapWidth = isotopeWrap.width();
            var isotopeWrapFoliosize = isotopeWrap.data('foliosize');

            if (jQuery(isotopeWrap).hasClass('portfolio_big')) isotopeWrapFoliosize = 3;
            if (jQuery(isotopeWrap).hasClass('portfolio_medium')) isotopeWrapFoliosize = 4;
            if (jQuery(isotopeWrap).hasClass('portfolio_mini')) isotopeWrapFoliosize = 6;
            if (jQuery(isotopeWrap).data('columns') != '0' && jQuery(isotopeWrap).data('columns')) isotopeWrapFoliosize = parseInt(jQuery(isotopeWrap).data('columns'));

            var isotopeItemIncw = '1';
            var isotopeItemInch = '1';

            //indent    
            var indent = false;
            if (jQuery(isotopeWrap).hasClass('isotopeIndent')) {
                indent = true;
                isotopeWrapWidth = jQuery(isotopeWrap).parents('.masonryWrap').width();
                jQuery(isotopeWrap).width(isotopeWrapWidth);
            }

            isotopeItem.each(function() {
                var isotopeItemIncw = jQuery(this).data('incw');
                var isotopeItemInch = jQuery(this).data('inch');
             //   if (jQuery(window).width() < 600) isotopeItemIncw = isotopeItemInch = 1;
                var isotopeSize = isotopeResizeMath(isotopeWrapWidth, isotopeWrapFoliosize, isotopeItemIncw, isotopeItemInch);

                jQuery(this).css({
                    'width': isotopeSize[0],
                    'height': isotopeSize[1]
                });

                //indent
                if (indent == true) {
                    jQuery(this).find('.isotopeItemWrap').css({
                        'width': (isotopeSize[0] - 30),
                        'height': (isotopeSize[1] - 30),
                        'margin-left': '15px'
                    });
                } else {
                    jQuery(this).find('.isotopeItemWrap').css({
                        'width': isotopeSize[0],
                        'height': isotopeSize[1]
                    });
                }

                var titleSize = 30;
                var excerptSize = 14;
                if (isotopeSize[0] > 600) {
                    titleSize = 30;
                    excerptSize = 14;
                }
                if (isotopeSize[0] < 600) {
                    titleSize = 30;
                    excerptSize = 14;
                }
                if (isotopeSize[0] < 500) {
                    titleSize = 24;
                    excerptSize = 14;
                }
                if (isotopeSize[0] < 400) {
                    titleSize = 20;
                    excerptSize = 12;
                }
                if (isotopeSize[0] < 300) {
                    titleSize = 18;
                    excerptSize = 12;
                    jQuery(this).find('.isotopeRating span.rInfo').css({
                        'padding': '2px 10px',
                        'font-size': '16px'
                    });
                    jQuery(this).find('.isotopeRating').css({
                        'padding': '0 3px 3px 3px',
                        'margin': '0 0 0 -30px'
                    });
                }
                if (isotopeSize[0] < 200) {
                    titleSize = 16;
                    excerptSize = 14;
                }
                if (jQuery(this).hasClass('hoverStyle_2')) titleSize = titleSize * 2;
                jQuery(this).find('.isotopeItemWrap .isotopeTitle').css({
                    'font-size': titleSize + 'px',
                    'line-height': '140%'
                });
                jQuery(this).find('.isotopeItemWrap .postInfo a').css({
                    'font-size': excerptSize + 'px',
                    'line-height': '140%'
                });
            });

            //isotope
            var isotopeSize = isotopeResizeMath(isotopeWrapWidth, isotopeWrapFoliosize, isotopeItemIncw, isotopeItemInch);
            isotopeWrap.isotope({
                layoutMode: 'masonry',
                resizable: false,
                filter: THEMEREX_isotopeFilter,
                masonry: {
                    columnWidth: isotopeSize[0]
                },
                itemSelector: '.isotopeItem',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            isotopeRow(isotopeWrap, isotopeItem);

            setTimeout(function() {
                isotoreEffect()
            }, 200);


            jQuery(window).smartresize(function() {
                "use strict";
                beforeIsotopeItemResize(isotopeWrap, isotopeWrap, isotopeItem, isotopeWrapFoliosize);
            });

            if (jQuery(isotopeWrap).find('[data-incw="2"]').length || jQuery(isotopeWrap).find('[data-inch="2"]').length) {
                jQuery(isotopeWrap).find('.isotopeMore').each(function() {
                    jQuery(this).css('display', 'none');
                });
            }

            //isotope Full post 
            isotopeWrap.on('click', 'article.isotopeItem:not(.noThumb):not(.hoverStyle_3)', function(e) {
                if (jQuery(this).hasClass('post_format_link')) {
                    location.href = jQuery(this).find('.isotopeLinks').attr('href');
                } else {
                    if (e.target.className == 'tag_link') return;
					var x = jQuery(this).find('.isotopeTitle a').attr('href');
					
					if(jQuery(this).hasClass('hoverStyle_1'))
					{
						e.preventDefault();
						e.stopPropagation();
						window.open(x, '_self');
					}
					else{
						window.location = x;
					}
                }
            });

            //isotope navigation
            isotopeWrap.on('click', '.isotopeNav', function() {
                var scrollPos = jQuery(window).scrollTop();
                var nav_id = jQuery(this).data('nav-id');
                jQuery('html,body').animate({
                    scrollTop: scrollPos
                }, 0);
                isotopeAjaxLoad(isotopeWrap, jQuery('.isotopeItem[data-postid="' + nav_id + '"]'));
            });


            //isotope Fullpost closed 
            isotopeWrap.on('click', '.fullItemClosed', function() {
                isotopeRemove(isotopeWrap, jQuery(this).parent('.fullItemWrap'));
            });



            //isotope filtre
            jQuery('.isotopeFiltr li a').click(function() {
                "use strict";

                isotopeRemove(isotopeWrap, isotopeWrap.find('.fullItemWrap'));

                jQuery('.isotopeFiltr li').removeClass('active');
                jQuery(this).parent().addClass('active');

                var selectorFilter = jQuery(this).attr('data-filter');

                isotopeFilterClass(selectorFilter);

                isotopeWrap.isotope({
                    layoutMode: 'masonry',
                    itemSelector: '.isotopeItem',
                    filter: selectorFilter,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                }).isotope('on', 'layoutComplete', function() {
                    isotopeRow(isotopeWrap, isotopeItem);
                });


                THEMEREX_isotopeFilter = selectorFilter;
                return false;
            });
        });

    }

    // main Slider
    if (jQuery('.sliderBullets, .sliderHeader').length > 0) {
        if (jQuery.rsCSS3Easing != undefined && jQuery.rsCSS3Easing != null) {
            jQuery.rsCSS3Easing.easeOutBack = 'cubic-bezier(0.175, 0.885, 0.320, 1.275)';
        }
        jQuery('.sliderHeader').addClass('hsInit');
        initShortcodes(jQuery(this));
    }


    // ====================================================================================
    // Page Navigation
    jQuery(document).click(function() {
        "use strict";
        jQuery('.pageFocusBlock').slideUp();
    });
    jQuery('.pageFocusBlock').click(function(e) {
        "use strict";
        e.preventDefault();
        return false;
    });
    jQuery('.navInput').click(function(e) {
        "use strict";
        jQuery('.pageFocusBlock').slideDown();
        e.preventDefault();
        return false;
    });

    //related links
    jQuery('.postBoxItem').click(function() {
        "use strict";
        var link = jQuery(this).find('h5 a').attr('href');
        if (link != '') {
            window.location.href = link;
        }
    })


    // topMenu DROP superfish
    jQuery('.topMenu ul, .usermenuArea ul').superfish({
        delay: 1000,
        animation: {
            opacity: 'show',
            height: 'show'
        },
        animationOut: {
            opacity: 'hide',
            height: 'hide'
        },
        speed: 'fast',
        autoArrows: false,
        dropShadows: false
    });



    // top menu animation
    jQuery(document).click(function() {
        "use strict";
        jQuery('.hideMenuDisplay #header').removeClass('topMenuShow');
    });
    jQuery('.hideMenuDisplay .wrapTopMenu').click(function(e) {
        "use strict";
        e.stopPropagation();
    });
    jQuery('.hideMenuDisplay .openTopMenu').click(function(e) {
        "use strict";
        e.stopPropagation();
        jQuery(this).parent().toggleClass('topMenuShow');
        return false;
    });




    // Sidemenu DROP
    jQuery('.sidemenu_area > ul > li.dropMenu ').click(function(e) {
        "use strict";
        e.preventDefault();
        return false;
    });
    jQuery('.sidemenu_area > ul > li.dropMenu, .sidemenu_area > ul > li.dropMenu li').click(function(e) {
        "use strict";
        initScroll('sidemenu_scroll');
        jQuery(this).toggleClass('dropOpen');
        jQuery(this).find('ul').first().slideToggle();
        e.preventDefault();
        return false;
    });

    jQuery('#sidemenu_scroll a').click(function(e) {
        "use strict";
        initScroll('sidemenu_scroll');
        jQuery('#sidemenu_scroll').mCustomScrollbar("update");
        e.preventDefault();
        return false;
    });

    jQuery(document).click(function(e) {
        "use strict";
        jQuery('body').removeClass('openMenuFixRight openMenuFix');
        jQuery('.sidemenu_overflow').fadeOut(400);
        jQuery('body').attr('style', '');;

    });
    jQuery('.sidemenu_wrap.swpLeftPos, .swpRightPos, .openRightMenu').click(function(e) {
        "use strict";
        e.preventDefault();
        return false;
    });

    jQuery('.sidemenu_wrap .sidemenu_button').click(function(e) {
        "use strict";
        jQuery('body').addClass('openMenuFix');
        if (jQuery('.sidemenu_overflow').length == 0) {
            jQuery('body').append('<div class="sidemenu_overflow"></div>')
        }
        jQuery('.sidemenu_overflow').fadeIn(400);
        jQuery('body').css('overflow', 'hidden');
        e.preventDefault();
        return false;
    });

    jQuery('.openRightMenu').click(function(e) {
        "use strict";
        jQuery('body').addClass('openMenuFixRight');
        if (jQuery('.sidemenu_overflow').length == 0) {
            jQuery('body').append('<div class="sidemenu_overflow"></div>')
        }
        jQuery('.sidemenu_overflow').fadeIn(400);
        jQuery('body').css('overflow', 'hidden');
        e.preventDefault();
        return false;
    });


    //Hover DIR
    jQuery(' .portfolio > .isotopeItem > .hoverDirShow').each(function() {
        "use strict";
        jQuery(this).hoverdir();
    });


    //Portfolio item Description
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        jQuery('.toggleButton').show();
        jQuery('.itemDescriptionWrap,.toggleButton').click(function(e) {
            "use strict";
            jQuery(this).toggleClass('descriptionShow');
            jQuery(this).find('.toggleDescription').slideToggle();
            e.preventDefault();
            return false;
        });
    } else {
        jQuery('.itemDescriptionWrap').hover(function() {
            "use strict";
            jQuery(this).toggleClass('descriptionShow');
            jQuery(this).find('.toggleDescription').slideToggle();
        })
    }


    jQuery('input[type="text"], input[type="password"], input[type="search"], textarea').focus(function() {
            "use strict";
            jQuery(this).attr('data-placeholder', jQuery(this).attr('placeholder')).attr('placeholder', '')
            jQuery(this).parent('li').addClass('iconFocus');
        })
        .blur(function() {
            "use strict";
            jQuery(this).attr('placeholder', jQuery(this).attr('data-placeholder'))
            jQuery(this).parent('li').removeClass('iconFocus');
        });

    //responsive Show menu
    jQuery('.openMobileMenu').click(function(e) {
        "use strict";
        var ul = jQuery('.wrapTopMenu .topMenu > ul');
        ul.slideToggle();
        jQuery(this).parents('.menuFixedWrap').toggleClass('menuMobileShow');
        e.preventDefault();
        return false;
    });


    // IFRAME width and height constrain proportions 
    if (jQuery('iframe').length > 0) {
        jQuery(window).resize(function() {
            "use strict";
            videoDimensions();
        });
        videoDimensions();
    }

    // Hide empty pagination
    if (jQuery('#nav_pages > ul > li').length < 3) {
        jQuery('#nav_pages').remove();
    } else {
        jQuery('.theme_paginaton a').addClass('theme_button');
    }

    // View More button
    jQuery('#viewmore_link').click(function(e) {
        "use strict";
        if (!THEMEREX_VIEWMORE_BUSY) {
            jQuery(this).addClass('loading');
            THEMEREX_VIEWMORE_BUSY = true;
            jQuery.post(THEMEREX_ajax_url, {
                action: 'view_more_posts',
                nonce: THEMEREX_ajax_nonce,
                page: THEMEREX_VIEWMORE_PAGE + 1,
                data: THEMEREX_VIEWMORE_DATA,
                vars: THEMEREX_VIEWMORE_VARS
            }).done(function(response) {
                "use strict";
                var rez = JSON.parse(response);
                jQuery('#viewmore_link').removeClass('loading');
                THEMEREX_VIEWMORE_BUSY = false;
                if (rez.error === '') {

                    var posts_container = jQuery('.content').eq(0);

                    if (posts_container.find('section.isotopeWrap').length > 0) posts_container = posts_container.find('section.isotopeWrap').eq(0);

                    if (posts_container.hasClass('isotopeWrap')) {
                        posts_container.append(rez.data);
                        THEMEREX_isotopeInitCounter = 0;
                        initAppendedIsotope(posts_container, rez.filters);
                    } else {
                        jQuery('.ajaxContainer').append(rez.data);
                    }

                    initPostFormats();
                    THEMEREX_VIEWMORE_PAGE++;
                    if (rez.no_more_data == 1) {
                        jQuery('#viewmore_link').hide();
                    }
                    if (jQuery('#nav_pages ul li').length >= THEMEREX_VIEWMORE_PAGE) {
                        jQuery('#nav_pages ul li').eq(THEMEREX_VIEWMORE_PAGE).toggleClass('pager_current', true);
                    }
                }
            });
        }
        e.preventDefault();
        return false;
    });

    // Infinite pagination
    if (jQuery('#viewmore_link.pagination_infinite').length > 0) {
        jQuery(window).scroll(infiniteScroll);
    }

    //custom panel scroll
    if (jQuery('#custom_options').length > 0) {
        jQuery('#custom_options .sc_scroll').css('height', jQuery('#custom_options').height() - 46);
    }

    // Scroll to top
    jQuery('.buttonScrollUp').click(function(e) {
        "use strict";
        jQuery('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
        return false;
    });


    jQuery('.sc_blogger.style_regular').each(function() {
        var isotopeWrapFoliosize = jQuery(this).data('columns');
        var isotopeWrap = jQuery(this);
        var isotopeItem = jQuery(this).find('.sc_columns_item');

        beforeIsotopeItemResize('regular', isotopeWrap, isotopeItem, isotopeWrapFoliosize);
        jQuery(window).resize(function() {
            beforeIsotopeItemResize('regular', isotopeWrap, isotopeItem, isotopeWrapFoliosize);
        });
    });

    initPostFormats();
	
}; //end ready



// Fit video frame to document width
function videoDimensions() {
    jQuery('iframe').each(function() {
        "use strict";
        var iframe = jQuery(this).eq(0);
        var w_attr = iframe.attr('width');
        var h_attr = iframe.attr('height');
        if (!w_attr || !h_attr) {
            return;
        }
        var w_real = iframe.width();
        if (w_real != w_attr) {
            var h_real = Math.round(w_real / w_attr * h_attr);
            iframe.height(h_real);
        }
    });
}

function initPostFormats() {
    "use strict";

    // MediaElement init
    if (THEMEREX_useMediaElement) {

        if (jQuery('audio').length > 0) {
            jQuery('audio').each(function() {
                if (jQuery(this).hasClass('inited')) return;
                jQuery(this).addClass('inited').mediaelementplayer({
                    audioWidth: '100%', // width of audio player
                    audioHeight: 30, // height of audio player
                    success: function(mediaElement, domObject) {
                        jQuery(domObject).parents('.sc_audio').addClass('sc_audio_show');
                    },
                });
            });
        }

        jQuery('video').each(function() {
            if (jQuery(this).hasClass('inited')) return;
            jQuery(this).addClass('inited').mediaelementplayer({
                videoWidth: -1, // if set, overrides <video width>
                videoHeight: -1, // if set, overrides <video height>
                audioWidth: '100%', // width of audio player
                audioHeight: 30 // height of audio player
            });
        });
    } else {
        jQuery('.sc_audio').addClass('sc_audio_show');
    }

    // Popup init image
    jQuery("a[href$='jpg']:not(.prettyphoto),a[href$='jpeg']:not(.prettyphoto),a[href$='png']:not(.prettyphoto),a[href$='gif']:not(.prettyphoto)").attr('rel', 'magnific');
    jQuery("a[rel*='magnific']:not(.inited)").addClass('inited').attr('data-effect', THEMEREX_MAGNIFIC_EFFECT_OPEN).magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: true,
        fixedContentPos: true,
        removalDelay: 500,
        midClick: true,
        preloader: true,
        gallery: {
            enabled: true
        },
        tLoading: '<span></span>',
        image: {
            tError: THEMEREX_MAGNIFIC_ERROR,
            verticalFit: true,
        },
        callbacks: {
            beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        }
    });
    // Popup init video
    jQuery("a[href*='youtube'],a[href*='vimeo']").attr('rel', 'magnific-video');
    jQuery("a[rel*='magnific-video']:not(.inited)").addClass('inited').attr('data-effect', THEMEREX_MAGNIFIC_EFFECT_OPEN).magnificPopup({
        type: 'iframe',
        closeOnContentClick: true,
        closeBtnInside: true,
        fixedContentPos: true,
        removalDelay: 500,
        midClick: true,
        preloader: true,
        callbacks: {
            open: function() {
                //open function
            },
            close: function() {
                //close function
            }
        }
    });



    // Popup windows with any html content
    jQuery('.user-popup-link:not(.inited)').addClass('inited').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = 'mfp-zoom-in';
                initShortcodes(jQuery('.sc_popup'));
            },
            open: function() {
                jQuery('html').css({
                    overflow: 'visible',
                    margin: 0
                });
            },
            close: function() {}
        },
        midClick: true
    });


    // Add video on thumb click
    jQuery('.sc_video_frame').each(function() {
        "use strict";
        if (jQuery(this).hasClass('sc_inited')) return;
        if (jQuery(this).hasClass('sc_video_frame_auto_play')) {
            scVideoAutoplay(jQuery(this));
        }
        jQuery(this).addClass('sc_inited').click(function(e) {
            "use strict";
            scVideoAutoplay(jQuery(this));
            e.preventDefault();
        });
    });
    jQuery('.sc_video_frame').hover(function() {
        jQuery(this).find('.sc_video_frame_player_title').slideDown(400);
    }, function() {
        "use strict";
        jQuery(this).find('.sc_video_frame_player_title').slideUp(400);
    });

    function scVideoAutoplay($videoObject) {
        "use strict";
        var video = $videoObject.data('videoframe');
        if (video !== '' && !$videoObject.hasClass('sc_video_active')) {
            $videoObject.addClass('sc_video_active');
            $videoObject.empty().html(video);
            videoDimensions();
        }
        return false;
    }

    //hover Underline effect
    jQuery('.hoverUnderline').each(function() {
        jQuery(this).find('a').each(function() {
            //    jQuery(this).append('<span class="hoverLine"></span>');
        });
    });

}

//mobile menu init, resize
function mobileMenuShow() {
    "use strict";
    if (THEMEREX_RESPONSIVE_MENU < jQuery(window).width()) {
        jQuery('.wrapTopMenu .topMenu > ul').removeAttr('style');
    }
}

// Infinite Scroll 
function infiniteScroll() {
    "use strict";
    var v = jQuery('#viewmore_link.pagination_infinite').offset();
    if (jQuery(this).scrollTop() + jQuery(this).height() + 100 >= v.top && !THEMEREX_VIEWMORE_BUSY) {
        jQuery('#viewmore_link').eq(0).trigger('click');
    }
}

//itemPageFull
function itemPageFull() {
    "use strict";
    var bodyHeight = jQuery(window).height();
    jQuery('.itemPageFull').css('height', bodyHeight - jQuery('.topWrap').height());
    jQuery('#sidemenu_scroll').css('height', bodyHeight);

    jQuery('.rev_slider .tp-caption').each(function() {
        if (jQuery(this).find('.rev_border').length > 0)
            jQuery(this).addClass('sc_rev_border');
        if (jQuery(this).find('.rev_border.rev_black').length > 0)
            jQuery(this).addClass('rev_black');
    });
}


//init scroll
function initScroll(idScroll) {
    "use strict";

    if (!jQuery('#' + idScroll).hasClass("scrollInit")) {
        jQuery('#' + idScroll).addClass('scrollInit').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
        });

        jQuery('.scrollPositionAction > .roundButton').click(function(e) {
            "use strict";
            var scrollAction = jQuery(this).data('scroll');
            jQuery('#' + idScroll).mCustomScrollbar("scrollTo", scrollAction);
            e.preventDefault();
            return false;
        });

    }
}


//scroll Action
function scrollAction() {
    "use strict";
    var head = jQuery('header');
    var buttonScrollTop = jQuery('.upToScroll');
    var scrollPos = jQuery(window).scrollTop();
    var headHeight = jQuery(window).height();
    var topMemuHeight = head.height();
    var menuMinWidth = jQuery(window).width() > jQuery('body').data('responsive');
    var menuMinWidth = head.find('.menuFixedWrap').height();

    //fixed menu
    if (scrollPos <= topMemuHeight / 3 && menuMinWidth) {
        head.removeClass('fixedTopMenuShow');
    } else if (scrollPos >= topMemuHeight / 1.5 && menuMinWidth) {
        head.addClass('fixedTopMenuShow');
        //smartScroll
        if (THEMEREX_REMEMBERSCROLL < scrollPos) {
            //scroll up
            head.removeClass('smartScrollDown');
            jQuery('.menuFixedWrap').height(menuMinWidth);
        } else if (THEMEREX_REMEMBERSCROLL > scrollPos) {
            //scroll down
            head.addClass('smartScrollDown');
            jQuery('.menuFixedWrap').height('auto');
        }

    }

    THEMEREX_REMEMBERSCROLL = scrollPos;

    //button UP 
    if (scrollPos > topMemuHeight) {
        buttonScrollTop.addClass('buttonShow');
    } else {
        buttonScrollTop.removeClass('buttonShow');
    }
}

function fullSlider() {
    "use strict";
    if (jQuery('.fullScreenSlider').length > 0) {
        jQuery('.sliderHeader, .sliderHeader .rsContent').css('height', jQuery(window).height())
    }
    if(jQuery(window).width() < 800)
    {
        var Sd = jQuery('.usermenuControlPanel').width() + 4;
        Sd = Sd * (-1);
        jQuery('#header .usermenuArea ul.usermenuList .usermenuCart ul').css('margin-right', Sd);
    }
}


//Time Line
function timelineResponsive() {
    "use strict";
    var bodyHeight = jQuery(window).height();
    var headHeight = jQuery(window).height() - jQuery('.contentTimeLine h2').height() - 150;
    var leftPosition = (jQuery('.main_content').width() - jQuery('.main').width()) / 2 + jQuery('.sidemenu_wrap').width();
    jQuery('.TimeLineScroll .tlContentScroll').css('height', headHeight);

}

//============= isotope function ============

//isotope effect
function isotoreEffect() {
    "use strict";
    var isotopeWrap = jQuery('.isotopeWrap ');
    isotopeWrap.find('.isotopeItem:not(.isotopeItemShow)').addClass('isotopeItemShow');
}

// isotope rows
function isotopeRow(itemWrap, item) {
    "use strict";

    var isotopeWrap = itemWrap;
    var isotopeItem = itemWrap.find('.isotopeItem:not(:hidden)');
    var i = 0;
    var positionCounter = 1;
    var items_sum = 0;
    var row_num = 1;
    var positionCounterArr = [];
    var isotope_width = isotopeWrap.width()

    item.removeClass('itemFirst itemLast').removeAttr('data-row-num');
    isotopeItem.filter(':visible:last').addClass('itemLast');

    itemWrap.find('.isotopeItem:not(:hidden)').each(function() {

        var item_l = jQuery(this).position().left;

        if (item_l == 0) {
            jQuery(this).addClass('itemFirst');
        }

    });

}

//scrolling
function isotopeScrolling(item) {
    "use strict";
    setTimeout(function() {
        jQuery('html,body').animate({
            scrollTop: item.offset().top + item.height() - 100
        }, 'slow');
    }, 2000);
}

//isotope Ajax Load
function isotopeAjaxLoad(itemWrap, item) {
    "use strict";

    var itemRow = item.data('row-num');
    var istPostID = item.data('postid');
    var navFirstID = item.parent('.isotopeWrap').find('article.isotopeItem:visible:first').data('postid');
    var navLastID = item.parent('.isotopeWrap').find('article.isotopeItem:visible:last').data('postid');
    var navPrevID = item.prevAll('article.isotopeItem:visible').data('postid');
    var navNextID = item.nextAll('article.isotopeItem:visible').data('postid');
    var isoFilter = THEMEREX_isotopeFilter.replace('.', '').replace('*', '');


    if (item.hasClass('isotopeActive')) {
        return;
    }

    jQuery('.isotopeItem[data-postid="' + THEMEREX_isotopeMemoryID + '"]').removeClass('isotopeActive')
    jQuery('.isotopeItem[data-postid="' + istPostID + '"]').addClass('isotopeActive');

    var itemContent = jQuery('<div class="fullItemWrap isotopeItem sc_loader_show ' + isoFilter + '" data-postid="' + istPostID + '"><span class="fullItemClosed icon-icon-26" title="Closed"></span><div class="fullContent"></div></div>');


    isotopeRemove(itemWrap, itemWrap.find('.fullItemWrap'));

    var next_before = item.nextAll('.itemFirst').eq(0);
    if (!item.hasClass('itemLast') && next_before.length > 0) {
        item.nextAll('.itemFirst:visible').eq(0).before(itemContent);
    } else {
        itemWrap.find('article.itemLast').after(itemContent);
    }

    //jQuery('.isotopeItem.itemLast[data-row-num="'+itemRow+'"]').after( itemContent );  

    itemWrap.isotope('destroy').isotope({
        //getSortData: {
        //  ids: '[data-postid]',
        //},
        //sortBy: ['ids'],
        layoutMode: 'masonry',
        itemSelector: '.isotopeItem',
        filter: THEMEREX_isotopeFilter,
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }

    }).isotope('on', 'layoutComplete', function() {
        //function complete
    });


    //add effect
    setTimeout(function() {
        "use strict";
        isotoreEffect();
    }, 500);

    isotopeScrolling(item)

    //load content 
    jQuery.post(THEMEREX_ajax_url, {
        action: 'isotope_content',
        nonce: THEMEREX_ajax_nonce,
        postID: istPostID,
    }).done(function(response) {
        "use strict";
        var rez = JSON.parse(response);
        jQuery('.fullItemWrap .fullContent').html((rez != '' ? rez.data : THEMEREX_SEND_ERROR)).parent('.fullItemWrap').addClass('ajaxShow');
        initShortcodes(jQuery('.fullItemWrap'));
        initPostFormats();

        //nav prev
        jQuery('.isotopeNav.isoPrev').data('nav-id', (navPrevID != undefined ? navPrevID : navLastID));
        jQuery('.isotopeNav.isoNext').data('nav-id', (navNextID != undefined ? navNextID : navFirstID));

        THEMEREX_isotopeInitCounter = 0;
        initRelayoutIsotope(jQuery('.fullItemWrap .fullContent'));
    });


    THEMEREX_isotopeMemoryID = istPostID;

    return false;
}


function isotopeFilterClass(selector) {
    "use strict";

    jQuery('.isotopeWrap .isotopeItem').removeClass('isotopeVisible').each(function() {
        if (selector == '*') {
            jQuery(this).addClass('isotopeVisible');
        } else {
            jQuery(selector).addClass('isotopeVisible');
        }
    });
}


//isotope remove
function isotopeRemove(itemWrap, item) {
    "use strict";

    var isotopeWrap = itemWrap;
    isotopeWrap.find('.isotopeItem[data-postid="' + THEMEREX_isotopeMemoryID + '"]').removeClass('isotopeActive')
    isotopeWrap.isotope('remove', item).isotope('layout');
}

//isotope Images Complete
function initRelayoutIsotope(content) {
    "use strict";
    if (!imagesCompleteLoad(content) && THEMEREX_isotopeInitCounter++ < 30) {
        setTimeout(function() {
            initRelayoutIsotope(content);
        }, 300);
        return;
    }
    jQuery('.isotopeWrap').isotope('layout');
}

//init Appended Isotope
function initAppendedIsotope(isotopeWrap, filters) {
    "use strict";
    if (!imagesCompleteLoad(isotopeWrap) && THEMEREX_isotopeInitCounter++ < 30) {
        setTimeout(function() {
            initAppendedIsotope(isotopeWrap, filters);
        }, 300);
        return;
    }

    var flt = isotopeWrap.siblings('.isotopeFiltr');
    var item = isotopeWrap.find('.isotopeItem:not(.isotopeItemShow)').addClass('isotopeItemShow');
    var isotopeWrapWidth = isotopeWrap.width();
    var isotopeItemWidth = isotopeWrap.data('foliosize');

    item.css('width', Math.floor(isotopeWrap.width() / Math.floor(isotopeWrap.width() / isotopeItemWidth)));

    isotopeRow(isotopeWrap, isotopeWrap.find('isotopeItem'))

    isotopeWrap.isotope('appended', item);
    for (var i in filters) {
        if (flt.find('a[data-filter=".flt_' + i + '"]').length == 0) {
            flt.find('ul').append('<li><a href="#" data-filter=".flt_' + i + '">' + filters[i] + '</a></li>');
        }
    }
}

function beforeIsotopeItemResize(itemWrap, isotopeWrap, isotopeItem, isotopeWrapFoliosize) {
    if (itemWrap != 'regular')
        if (itemWrap.find('.fullItemWrap').length > 0) {
            isotopeRemove(itemWrap, itemWrap.find('.fullItemWrap'));
        }

    var isotopeWrapWidth = isotopeWrap.width();
    if (isotopeWrapWidth == 0) isotopeWrapWidth = isotopeWrap.parent().width();
    var indent = false;
    if (jQuery(isotopeWrap).hasClass('isotopeIndent')) {
        indent = true;
        if (itemWrap != 'regular') {

            isotopeWrapWidth = jQuery(isotopeWrap).parents('.masonryWrap').width();
            jQuery(isotopeWrap).width(isotopeWrapWidth);
        }
        //  else isotopeWrapWidth = jQuery(isotopeWrap).width();

    }

    isotopeItem.each(function() {
        "use strict";
        var isotopeItemIncw = jQuery(this).data('incw');
        var isotopeItemInch = jQuery(this).data('inch');
        if (itemWrap == 'regular') isotopeItemIncw = isotopeItemInch = 1;
        var isotopeSize = isotopeResizeMath(isotopeWrapWidth, isotopeWrapFoliosize, isotopeItemIncw, isotopeItemInch);

        jQuery(this).css({
            'width': isotopeSize[0],
            'height': isotopeSize[1]
        });

        //indent
        if (itemWrap != 'regular') {
            if (indent == true) {
                jQuery(this).find('.isotopeItemWrap').css({
                    'width': (isotopeSize[0] - 30),
                    'height': (isotopeSize[1] - 30),
                    'margin-left': '15px'
                });
            } else {
                jQuery(this).find('.isotopeItemWrap').css({
                    'width': isotopeSize[0],
                    'height': isotopeSize[1]
                });
            }
        } else {
            if (indent == true) {
                jQuery(this).css({
                    'width': (isotopeSize[0] - 30)
                });
            } else {
                jQuery(this).css({
                    'width': isotopeSize[0]
                });
            }
        }

        var titleSize = 30;
        var excerptSize = 14;
        if (isotopeSize[0] > 600) {
            titleSize = 30;
            excerptSize = 14;
        }
        if (isotopeSize[0] < 600) {
            titleSize = 30;
            excerptSize = 14;
        }
        if (isotopeSize[0] < 500) {
            titleSize = 24;
            excerptSize = 14;
        }
        if (isotopeSize[0] < 400) {
            titleSize = 20;
            excerptSize = 12;
        }
        if (isotopeSize[0] < 300) {
            titleSize = 18;
            excerptSize = 12;
            jQuery(this).find('.isotopeRating span.rInfo').css({
                'padding': '2px 10px',
                'font-size': '16px'
            });
            jQuery(this).find('.isotopeRating').css({
                'padding': '0 3px 3px 3px',
                'margin': '0 0 0 -30px'
            });
        }
        if (isotopeSize[0] < 200) {
            titleSize = 16;
            excerptSize = 14;
        }
        if (jQuery(this).hasClass('hoverStyle_2')) titleSize = titleSize * 2;
        jQuery(this).find('.isotopeItemWrap .isotopeTitle').css({
            'font-size': titleSize + 'px',
            'line-height': '120%'
        });
        jQuery(this).find('.isotopeItemWrap .postInfo a').css({
            'font-size': excerptSize + 'px',
            'line-height': '140%'
        });

    });
    if (itemWrap != 'regular') {
        var isotopeSize = isotopeResizeMath(isotopeWrapWidth, isotopeWrapFoliosize, 1, 1);
        initRelayoutIsotope(itemWrap);
        isotopeWrap.isotope({
            layoutMode: 'masonry',
            resizable: false,
            filter: THEMEREX_isotopeFilter,
            masonry: {
                columnWidth: isotopeSize[0]
            },
            itemSelector: '.isotopeItem',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
    }
}

function isotopeResizeMath(isotopeWrapWidth, isotopeWrapFoliosize, incw, inch) {
    "use strict";

    var windowWidth = jQuery(window).width();
    if (windowWidth <= 1200 && isotopeWrapFoliosize > 4) isotopeWrapFoliosize = 4;
    if (windowWidth <= 900 && isotopeWrapFoliosize > 2) isotopeWrapFoliosize = 2;
    if (windowWidth <= 600 && isotopeWrapFoliosize > 1) isotopeWrapFoliosize = 1;
    if (windowWidth <= 900 && incw > 2) incw = 2;
    if (windowWidth <= 900 && inch > 2) inch = 2;

    if (windowWidth <= 600 && incw > 1) incw = 1;
    if (windowWidth <= 600 && inch == 1) inch = 0.5;
    if (windowWidth <= 600 && inch > 1) inch = 1;

    var bw = isotopeWrapWidth / isotopeWrapFoliosize;

    var w_px = (bw * incw) - 0.01 * incw;
    var h_px = (bw * inch) - 0.001 * inch;
    var w_pr = (100 / isotopeWrapFoliosize) * incw;
    var data = [w_px, h_px, w_pr]

    return data;
}


//isotope Images Complete
function imagesCompleteLoad(content) {
    "use strict";

    var complete = true;
    content.find('img').each(function() {
        if (!complete) return;
        if (!jQuery(this).get(0).complete) complete = false;
    });
    return complete;
}