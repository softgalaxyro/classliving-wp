<?php
// Redefine colors in styles
$THEMEREX_custom_css = "";
$res_layouts = get_theme_option('responsive_layouts') == 'yes';

function getThemeCustomStyles() {
	global $THEMEREX_custom_css;
	return $THEMEREX_custom_css;
}

function addThemeCustomStyle($style) {
	global $THEMEREX_custom_css;
	$THEMEREX_custom_css .= " {$style} \r\n";
}

//Custom heading H1-H6
function typography_header( $width='', $cutter=1){

	$header_size = array(
			1 => array( 1=>1, 2=>1, 3=>1, 4=>1, 5=>1, 6=>1),
			2 => array( 1=>1, 2=>1, 3=>1, 4=>1, 5=>1, 6=>1),
			3 => array( 1=>1, 2=>1, 3=>1, 4=>1, 5=>1, 6=>1)
	);
	$media = $width != '';
	$hCounter = 1;
	$media ? addThemeCustomStyle('@media (max-width: '.$width.'px) {') : '' ;
	while( $hCounter <= 6 ){
		$heading_array = array();
		$heading_array[] = 'font-size:'.getCssValue( get_custom_option('header_font_size_h'.$hCounter)/$header_size[$cutter][$hCounter]).'; ';
		$heading_array[] = (get_custom_option('header_font_spacing_h'.$hCounter) != '' ? 'letter-spacing:'.getCssValue(get_custom_option('header_font_spacing_h'.$hCounter)).'; ' : '' );
		$heading_array[] = get_custom_option('header_font_uppercase_h'.$hCounter) == 'yes' ? 'text-transform: uppercase;' : '';
		$heading_array[] = 'font-style:'.get_custom_option('header_font_style_h'.$hCounter).';';
		$heading_array[] = 'font-weight:'.get_custom_option('header_font_weight_h'.$hCounter).';';
		$heading_array[] = 'line-height:'.getCssValue(get_custom_option('header_line_height_h'.$hCounter)).';';

		$extra_h2 = $hCounter == 4 ? ', .sc_video_frame .sc_video_frame_info_wrap .sc_video_frame_info .sc_video_frame_player_title' : '';

		addThemeCustomStyle('h'.$hCounter.$extra_h2.'{ '.( !empty($heading_array) ? join(' ', $heading_array) : '').' }');
		$hCounter++;
	}
	$media ? addThemeCustomStyle('}') : '' ;
}

function prepareThemeCustomStyles() {

	// Custom font
	$fonts = getThemeFontsList(false);
	$theme_font = get_custom_option('theme_font');
	$header_font = get_custom_option('header_font');

	$theme_color = get_custom_option('theme_color');
	$theme_accent_color = get_custom_option('theme_accent_color');
	$background_color = get_custom_option('bg_color');

	$logo_widht = get_custom_option('logo_block_width');

	//theme fonts
	if (isset($fonts[$theme_font])) {
		addThemeCustomStyle('
			body, button, input, select, textarea { font-family: \''.$theme_font.'\', '.$fonts[$theme_font]['family'].'; }'); 
	}
	// heading fonts
	if (isset($fonts[$header_font])) {
		addThemeCustomStyle(
			(get_theme_option('show_theme_customizer') == 'yes' ? '.custom_options .co_label, .custom_options .co_header span, ' : '').
			'
			
			.subCategory .categoryTitle, .sideBarShow .postTitle,
			.sc_toggl .sc_toggl_item .sc_toggl_title,
			.sc_table table thead tr th,
			.sc_button,
			.sc_quote_title,
			.wp-caption .wp-caption-text, .sc_image .sc_image_caption,
			.sc_infobox_title,
			.sc_team.sc_team_item_style_1 .sc_team_item_position,
			.sc_testimonials .sc_testimonials_item_author .sc_testimonials_item_user,
			.sc_skills_arc .sc_skills_legend li,
			.postInfo .postSpan,
			.post.format-link a > .strong > p,
			.widget_trex_post .post_item .post_wrapper .post_info,
			.widget_search .searchFormWrap .searchSubmit input,
			.comments .commentInfo,
			.postBox .postBoxItem .postBoxInfo .postBoxCategory a,
			.isotopeWrap .isotopeItem .isotopeContent .isotopeExcerpt,
			.sc_familly_2,
			.isotopeWrap .isotopeItem.hoverStyle_3 .isotopeContent .show_more a,
			.sc_blogger_date,
			.widget_trex_post .ui-tabs-nav li a, .widget_top10 .ui-tabs-nav li a,
			.widget_recent_reviews .post_item .post_wrapper .post_info .post_review .post_review_number,
			a.sc_text,
			.sc_team .sc_team_item .sc_team_item_socials ul li.name,
			.wrapTopMenu .topMenu > ul > li > ul li a,
			.usermenuArea > ul > li > ul li a,
			#header .searchFormWrap span, 
			pre		{ font-family: \''.$header_font.'\',\''.$theme_font.'\', '.$fonts[$header_font]['family'].';}

			.sc_skills_arc .sc_skills_arc_canvas tspan{ font-family: \''.$header_font.'\', '.$fonts[$header_font]['family'].' !important;  font-size: 30px;}
			
			.woocommerce ul.cart_list li span.amount, .woocommerce ul.product_list_widget li span.amount,
			.woocommerce .woocommerce-breadcrumb a,
			.woocommerce .woocommerce-ordering, .woocommerce .woocommerce-result-count,
			.woocommerce ul.products li.product .onsale,
			.woocommerce .widget_price_filter .price_slider_amount .button,
			.woocommerce .woocommerce-breadcrumb,
			div.product form.cart .button,
			.woocommerce span.onsale, .woocommerce-page span.onsale,
			.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta, 
			.woocommerce-page #reviews #comments ol.commentlist li .comment-text p.meta,
			.woocommerce table thead tr th{ font-family: \''.$header_font.'\',\''.$theme_font.'\', '.$fonts[$header_font]['family'].';}
			'); 
			
	}
	
	typography_header();
	typography_header(1015,2);
	typography_header(449,3);
		

	//Custom logo style
	if( get_custom_option('logo_type') == 'logoImage'){
		//images style
		addThemeCustomStyle('
			.wrap.logoImageStyle .logoHeader{ width:'.$logo_widht.'px; }' );
	} else {
		//logo text style
		$style_logo_array  = array();
		$style_logo_array[] = 'font-family:"'.get_custom_option('logo_font').'";'; 
		$style_logo_array[] = 'font-style:'.get_custom_option('logo_font_style').';'; 
		$style_logo_array[] = 'font-weight:'.get_custom_option('logo_font_weight').';'; 
		$style_logo_array[] = 'font-size:'.get_custom_option('logo_font_size').'px;'; 
		$style_logo_array[] = 'line-height:'.get_custom_option('logo_font_size').'px;'; 
		addThemeCustomStyle('
			.wrap.logoTextStyle .logoHeader{ width:'.$logo_widht.'px; '.(!empty($style_logo_array) ? join(' ', $style_logo_array) : '').' } ');
	}

	//background custom style
	if( get_custom_option('body_style') == 'boxed'){
		$style_custom_array  = array();
		get_custom_option('bg_color') != '' ? $style_custom_array[] = get_custom_option('bg_color') : '';
		if ( get_custom_option('bg_custom_image') != ''){
			$style_custom_array[] = 'url('.get_custom_option('bg_custom_image').')' ;
			$style_custom_array[] = get_custom_option('bg_custom_image_position_x');
			$style_custom_array[] = get_custom_option('bg_custom_image_position_y');
			$style_custom_array[] = get_custom_option('bg_custom_image_repeat');
			$style_custom_array[] = get_custom_option('bg_custom_image_attachment');
		}
		addThemeCustomStyle('
			.wrap{ background-color: '.(!empty($style_custom_array) ? join(' ', $style_custom_array) : '').';}');
	}

	//theme color
	if($theme_color != ''){
		addThemeCustomStyle('
		/*color*/
		a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover,
		.h1 a:hover,.h2 a:hover,.h3 a:hover,.h4 a:hover,.h5 a:hover,.h6 a:hover,
		.logoHeader a, .subTitle, 
		#header .rightTop a,
		.menuStyle2 .wrapTopMenu .topMenu > ul > li > ul li.sfHover > a,
		.menuStyle2 .wrapTopMenu .topMenu > ul > li > ul li a:hover,
		.menuStyle2 .wrapTopMenu .topMenu > ul > li > ul li.menu-item-has-children:after,
		.widgetWrap ul > li a:hover,
		.widget_recent_comments ul > li a,
		.widget_twitter ul > li:before,
		.widget_twitter ul > li a,
		.widget_rss ul li a,
		.widget_trex_post .ui-tabs-nav li a,
		.comments .commentModeration .icon,
		.sc_button.sc_button_skin_global.sc_button_style_line,
		.sc_toggl.sc_toggl_style_2 .sc_toggl_item .sc_toggl_title:hover,
		.sc_tabs.sc_tabs_style_2 ul li a,
		.sc_tabs.sc_tabs_style_3 ul li.ui-tabs-active a,
		.sc_tabs.sc_tabs_style_3 ul.sc_tabs_titles li.ui-tabs-active a,
		.sc_blogger.style_list li a:hover,
		ul.sc_list.sc_list_style_iconed li:before,
		ul.sc_list.sc_list_style_iconed.sc_list_marked_yes li,
		ul.sc_list.sc_list_style_iconed li.sc_list_marked_yes ,
		.sc_button.sc_button_skin_global.sc_button_style_line,
		.sc_team.sc_team_item_style_1 .sc_team_item_title,
		.sc_team.sc_team_item_style_2 .sc_team_item_position,
		.sc_countdown.sc_countdown_round .sc_countdown_counter .countdown-section .countdown-amount,
		.sc_countdown .flip-clock-wrapper ul li a div div.inn,
		.sc_contact_info .sc_contact_info_wrap .sc_contact_info_lable,
		.isotopeWrap .fullItemWrap .fullItemClosed:hover,
		.postInfo .postReview .revBlock .ratingValue,
		.postBox .postBoxItem .postBoxInfo h5 a:hover,
		.wrapTopMenu .topMenu > ul > li > a:hover,
		.sc_audio_title,
		.sc_button.sc_button_skin_dark.sc_button_style_bg:hover,
		.sc_quote_title,
		.sc_list.sc_list_style_ul li:before,
		.sc_skills_counter .sc_skills_item.sc_skills_style_2 .sc_skills_count .sc_skills_total,
		.postInfo .postCategory a:hover,
		.postInfo .postDate a:hover,
		.widget_tag_cloud a:hover,
		.postBox .postBoxItem .postBoxInfo .postBoxCategory a:hover,
		.isotopeWrap .isotopeItem .isotopeContent .postInfo .postSpan.postTags a:hover,
		.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeTitle a,
		.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeTitle a:hover,
		.isotopeWrap .isotopeItem.hoverStyle_3 .isotopeContent .show_more a:hover,
		.sc_blogger.style_image  .sc_button,
		.sc_slider.sc_slider_dark .slider-control-nav li a:hover,
		.widget_top10 .ui-tabs-nav li a,
		.widget_recent_reviews .post_item .post_wrapper .post_info .post_review,
		.woocommerce .widget_price_filter .price_slider_amount .button:hover,
		.widgetWrap .tagcloud a:hover,
		a.checkout-button.button.alt.wc-forward:hover,
		.custom_socials li a:hover,
		.post.format-link a > .strong > p:hover,
		.openMobileMenu:before, .openMobileMenu:after	{color: '.$theme_color.';}

		/*background*/
		#header .openTopMenu,
		.menuStyle1 .wrapTopMenu .topMenu > ul > li > ul,
		.menuStyle1 .wrapTopMenu .topMenu > ul > li > ul > li ul,
		.menuStyle2 .wrapTopMenu .topMenu > ul > li > ul li a:before,
		.menuStyle1 #header ul > li > ul,
		.menuStyle1 #header ul > li > ul > li ul,
		.widget_calendar table tbody td a:before,
		.sc_toggl.sc_toggl_style_2.sc_toggl_icon_show .sc_toggl_item .sc_toggl_title:after,
		.sc_toggl.sc_toggl_style_3 .sc_toggl_item .sc_toggl_title ,
		.sc_table.sc_table_style_2 table thead tr th,
		.sc_highlight.sc_highlight_style_2,
		.sc_scroll .sc_scroll_bar .swiper-scrollbar-drag,
		.sc_skills_bar.sc_skills_vertical .sc_skills_item .sc_skills_count ,
		.sc_icon.sc_icon_box,
		.sc_icon.sc_icon_box_circle,
		.sc_icon.sc_icon_box_square,
		.sc_tabs.sc_tabs_style_2 ul.sc_tabs_titles li.ui-tabs-active a,
		.sc_slider.sc_slider_dark .slider-pagination-nav span.swiper-active-switch ,
		.sc_slider.sc_slider_light .slider-pagination-nav span.swiper-active-switch,
		.sc_testimonials.sc_testimonials_style_2 .sc_slider_swiper.sc_slider_pagination .slider-pagination-nav span.swiper-active-switch,
		.sc_blogger.style_date .sc_blogger_item:before,
		.sc_toggl.sc_toggl_style_2.sc_toggl_icon_show .sc_toggl_item .sc_toggl_title:after ,
		.sc_toggl.sc_toggl_style_3 .sc_toggl_item .sc_toggl_title ,
		.sc_dropcaps.sc_dropcaps_style_1 .sc_dropcap,
		.postInfo .postReview .revBlock.revStyle100 .ratingValue,
		.reviewBlock .reviewTab .revWrap .revBlock.revStyle100 .ratingValue,
		.post-password-required .post-password-form input[type="submit"]:hover,
		.sc_skills_counter .sc_skills_item.sc_skills_style_3 .sc_skills_count,
		.sc_skills_counter .sc_skills_item.sc_skills_style_4 .sc_skills_count,
		.sc_pricing_table.sc_pricing_table_style_3 .sc_pricing_item ul li.sc_pricing_title,
		.price_slider_amount button.button{ background-color: '.$theme_color.'; }

		
		/*border*/
		.nav_pages ul li a:hover,
		.menuStyle1 .wrapTopMenu .topMenu > ul > li > ul > li ul,
		.menuStyle2 .wrapTopMenu .topMenu > ul > li > ul > li ul,
		.widget_trex_post .ui-tabs-nav li a,
		.widget_top10 .ui-tabs-nav li a,
		.sc_button.sc_button_skin_global.sc_button_style_line,
		.sc_tabs.sc_tabs_style_2 ul li a,
		.sc_tabs.sc_tabs_style_2 ul li + li a,
		.sc_tabs.sc_tabs_style_2 ul.sc_tabs_titles li.ui-tabs-active a,
		.sc_tabs.sc_tabs_style_3 ul.sc_tabs_titles li.ui-tabs-active a,
		.sc_tabs.sc_tabs_style_2 .sc_tabs_array,
		.sc_tabs.sc_tabs_style_3 ul li.ui-tabs-active a,
		.sc_tabs.sc_tabs_style_3 .sc_tabs_array,
		.sc_blogger.style_date .sc_blogger_item .sc_blogger_date,
		.sc_banner:before,
		.sc_button.sc_button_skin_global.sc_button_style_line{ border-color: '.$theme_color.'; }


		 input[type="search"]::-webkit-search-cancel-button{color: '.$theme_color.';}
		.buttonScrollUp { border-color: transparent transparent '.$theme_color.' transparent ; }
		
		::selection { color: #fff; background-color:'.$theme_color.';}
		::-moz-selection { color: #fff; background-color:'.$theme_color.';}
		a.sc_icon:hover{ background-color: '.$theme_color.' !important;}
		');
	}


	if( $theme_accent_color != ''){
		addThemeCustomStyle('
			.themeDark .isotopeFiltr ul li.active a,
			.isotopeFiltr ul li a,
			.sc_toggl .sc_toggl_item.sc_active .sc_toggl_title,
			.sc_tabs ul.sc_tabs_titles li,
			.sc_table.sc_table_style_1 table thead tr th,
			.sc_button.sc_button_skin_global.sc_button_style_bg,
			.sc_dropcaps.sc_dropcaps_style_1 .sc_dropcap,
			.sc_highlight.sc_highlight_style_1,
			.sc_skills_bar .sc_skills_item .sc_skills_count,
			.sc_testimonials.sc_testimonials_style_2 .sc_testimonials_title:after,
			.sc_testimonials.sc_testimonials_style_2 .sc_slider_swiper.sc_slider_pagination .slider-pagination-nav span.swiper-active-switch,
			.sc_testimonials.sc_testimonials_style_1 .sc_testimonials_item_quote,
			.sc_pricing_table.sc_pricing_table_style_2 .sc_pricing_item ul,
			.sc_pricing_table.sc_pricing_table_style_2 .sc_pricing_item ul li.sc_pricing_title,
			.sc_skills_counter .sc_skills_item.sc_skills_style_3 .sc_skills_count,
			.sc_skills_counter .sc_skills_item.sc_skills_style_4 .sc_skills_count,
			.sc_skills_counter .sc_skills_item.sc_skills_style_4 .sc_skills_info:before,
			.sc_video_frame.sc_video_active:before,
		    .sc_loader_show:before,
			.widget_calendar table tbody td a:before,
			.widget_calendar table tbody td a:hover,
			.isotopeWrap .isotopeItem.hoverStyle_1 .isotopeItemWrap,
			.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeExcerpt,
			.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
			.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle,
			.woocommerce div.product .woocommerce-tabs ul.tabs li,
			.woocommerce table thead tr th,
			.woocommerce #content table.cart td.actions input.button, 
			.woocommerce table.cart td.actions input.button, 
			.woocommerce-page #content table.cart td.actions input.button, 
			.woocommerce-page table.cart td.actions input.button,
			.wrapTopMenu .topMenu > ul > li > ul li a:hover,
			.usermenuArea > ul > li > ul li a:hover{ background-color: '.$theme_accent_color.' }
			
			.sc_audio_author,
			.sc_team.sc_team_item_style_2 .sc_team_item .sc_team_item_socials ul li a span,
			.sc_button.sc_button_skin_global.sc_button_style_bg:hover,
			.sc_button.sc_button_skin_dark.sc_button_style_line:hover,
			.sc_button.sc_button_skin_global.sc_button_style_line:hover,
			.sc_dropcaps.sc_dropcaps_style_3 .sc_dropcap,
			.sc_testimonials.sc_testimonials_style_2 .sc_testimonials_title,
			.sc_testimonials.sc_testimonials_style_2 .sc_testimonials_item_quote .sc_testimonials_item_text_before:before,
			.sc_testimonials.sc_testimonials_style_2 .sc_testimonials_item_author .sc_testimonials_item_user,
			.reviewBlock .reviewTab .revWrap .revBlock .ratingValue,
			.nav_pages ul li a:hover,
			.woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before,
			.woocommerce table.cart td.actions input.button:hover,
			.woocommerce-page #content table.cart td.actions input.button:hover,
			.woocommerce-page table.cart td.actions input.button:hover,
			.woocommerce a.button:hover,
			div.product form.cart .button:hover,
			.woocommerce #review_form #respond .form-submit input:hover,
			.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeExcerpt:hover,
			.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeExcerpt:hover:after{ color: '.$theme_accent_color.' }
			
			
			
			.mejs-controls .mejs-time-rail .mejs-time-current,
			.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current{ background-color: '.$theme_accent_color.' !important;}
			
			.mejs-controls .mejs-button button:before { color: '.$theme_accent_color.' !important;}
			.sc_testimonials.sc_testimonials_style_2{ border-color: '.$theme_accent_color.';}
			.sc_testimonials.sc_testimonials_style_1 .sc_testimonials_item_author_show .sc_testimonials_item_quote:after { border-left-color: '.$theme_accent_color.'; }
			.isotopeWrap .isotopeItem.hoverStyle_2 .isotopeContent .isotopeExcerpt{ outline-color: '.$theme_accent_color.';}
			
			');
	}

	if( $background_color != ''){
		addThemeCustomStyle('{ background-color: '.$background_color.' }');
	}
	
	// Custom menu
	if (get_theme_option('menu_colored')=='yes') {
		$menu_name = 'mainmenu';
		if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
			$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
			if (is_object($menu) && $menu) {
				$menu_items = wp_get_nav_menu_items($menu->term_id);
				$menu_styles = '';
				$menu_slider = get_theme_option('menu_slider')=='yes';
				if (count($menu_items) > 0) {
					foreach($menu_items as $k=>$item) {
		//				if ($item->menu_item_parent==0) {
							$cur_accent_color = '';
							if ($item->type=='taxonomy' && $item->object=='category') {
								$cur_accent_color = get_category_inherited_property($item->object_id, 'theme_accent_color');
							}
							if ((empty($cur_accent_color) || is_inherit_option($cur_accent_color)) && isset($item->classes[0]) && !empty($item->classes[0])) {
								$cur_accent_color = (themerex_substr($item->classes[0], 0, 1)!='#' ? '#' : '').$item->classes[0];
							}
							if (!empty($cur_accent_color) && !is_inherit_option($cur_accent_color)) {
								$menu_styles .= ($item->menu_item_parent==0 ? "#header_middle_inner #mainmenu li.menu-item-{$item->ID}.current-menu-item > a," : '')
									. "
									#header_middle_inner #mainmenu li.menu-item-{$item->ID} > a:hover,
									#header_middle_inner #mainmenu li.menu-item-{$item->ID}.sfHover > a { background-color: {$cur_accent_color} !important; }
									#header_middle_inner #mainmenu li.menu-item-{$item->ID} ul { background-color: {$cur_accent_color} !important; } ";
							}
							if ($menu_slider && $item->menu_item_parent==0) {
								$menu_styles .= "
									#header_middle_inner #mainmenu li.menu-item-{$item->ID}.blob_over:not(.current-menu-item) > a:hover,
									#header_middle_inner #mainmenu li.menu-item-{$item->ID}.blob_over.sfHover > a { background-color: transparent !important; } ";
							}
		//				}
					}
				}
				if (!empty($menu_styles)) {
					addThemeCustomStyle($menu_styles);
				}
			}
		}
	}
	
	//main menu responsive width
	
	$menu_responsive = get_theme_option('responsive_menu_width').'px';
	addThemeCustomStyle("
		@media (max-width: {$menu_responsive}) { 
			.openMobileMenu { display: block }
			.menuStyleFixed #header.fixedTopMenuShow .menuFixedWrap { position: static !important }
			.wrapTopMenu .topMenu { width: 100% }
			.wrapTopMenu .topMenu > ul { display: none; border-top: 1px solid #fff; clear: both; }
			.wrapTopMenu .topMenu > ul li { clear: both }
			.wrapTopMenu .topMenu > ul li a { }
			.wrapTopMenu .topMenu > ul > li > ul:before { display: none }
			.openTopMenu { display: none }
			.wrapTopMenu .topMenu > ul > li.sfHover > a:before, .wrapTopMenu .topMenu > ul > li > a { opacity: 1 !important }
			.wrapTopMenu .topMenu > ul > li > a:hover:before { left: 10px; right: 10px; }
			.hideMenuDisplay .wrapTopMenu { min-height: 45px !important; height: auto !important; }
			.hideMenuDisplay .usermenuArea > ul li a { color: #fff !important }
			.menuStyleAbsolute #header { position: relative }
			#header .usermenuArea { position: absolute; right: 0px; top: 50%; margin-top: -26px; padding: 0; }
			.menuSmartScrollShow.menuStyleFixed #header.fixedTopMenuShow.smartScrollDown .menuFixedWrap { background: transparent }
			.custom_options_shadow, .custom_options { display: none !important }
			#header .searchform { display: none }
			#header { min-height: inherit; min-height: 100px; text-align: center; padding: 0 0 30px 0; }
			.wrap.logoImageStyle .logoHeader { margin: 0 auto; width: auto; position: relative; height: 100%; display: inline-block; overflow: hidden; }
			#header .usermenuArea { position: absolute; right: 0px; margin-top: -60px; padding: 0; top: auto; }
			.logoHeader img { height: 100px; width: auto; }
			.openMobileMenu { display: block; top: auto; margin-top: -60px; }
			#header .usermenuArea > ul.usermenuList .usermenuControlPanel > ul { right: 0; left: auto; margin: 0; }
			#header .usermenuArea ul.usermenuList .usermenuCart ul { left: auto; right: 0; width: 240px; }
			#header .usermenuArea > ul.usermenuList .usermenuControlPanel > ul { right: 0; left: auto; margin: 0; }
			#header .usermenuArea ul.usermenuList .usermenuCart .widget_area p.buttons a { padding: 10px 6px }
			#header { min-height: 60px }
			#header .usermenuArea ul.usermenuList .usermenuCart ul { left: auto; right: 0; width: 300px; }
			.usermenuArea > ul > li > a { padding: 0 3px }
			#header .usermenuArea { margin-top: -70px }
			.usermenuArea .cart_total .cart_icon:before { font-size: 16px }
			.usermenuArea > ul > li > a { font-size: 10px }
			.wrapTopMenu .topMenu > ul li { display: block; border-bottom: 1px solid #ddd; }
			.menuMobileShow .wrapTopMenu .topMenu > ul { text-align: left }
			.wrapTopMenu .topMenu > ul > li > ul { width: auto; margin: 0; position: static !important; margin-bottom: -1px; padding: 10px 30px; }
			.wrapTopMenu .topMenu > ul > li > ul li + li { border-top: 1px solid #ddd }
			.wrapTopMenu .topMenu > ul > li > ul li { border: 0 }
			.wrapTopMenu .topMenu > ul > li > ul li + li { border-top: 1px solid #ddd }
			.wrapTopMenu .topMenu > ul > li > ul li { list-style: none; text-align: left; position: relative; display: block; border: 0; }
			.wrapTopMenu .topMenu > ul > li > ul li a:hover { border: 0; background: none; border-bottom: 0; }
			.wrapTopMenu .topMenu > ul > li > ul li a { border: 0 }
		 }
	");



	// Main menu height
	$menu_height = (int) get_theme_option('menu_height');
	if ($menu_height > 20) {
		addThemeCustomStyle("
			#mainmenu > li > a { height: {$menu_height}px !important; line-height: {$menu_height}px !important; }
			#mainmenu > li ul { top: {$menu_height}px !important; }
			#header_middle { min-height: {$menu_height}px !important; } ");
	}
	// Submenu width
	$menu_width = (int) get_custom_option('menu_width');
	if ($menu_width > 50) {
		addThemeCustomStyle('
			#mainmenu > li:nth-child(n+6) ul li ul { left: -'.($menu_width).'px; } ');
	}

	//woocommerce
	if( function_exists('is_woocommerce') ){
	addThemeCustomStyle('
		/*wooc bg*/
		.woocommerce #content input.button, 
		.woocommerce #respond input#submit, 
		.woocommerce a.button, 
		.woocommerce button.button, 
		.woocommerce input.button, 
		.woocommerce-page #content input.button, 
		.woocommerce-page #respond input#submit, 
		.woocommerce-page a.button, 
		.woocommerce-page button.button, 
		.woocommerce-page input.button,
		.woocommerce #content div.product .woocommerce-tabs ul.tabs li,
		.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li	{font-family: \''.$header_font.'\',\''.$theme_font.'\', '.$fonts[$header_font]['family'].'; }
		
		.woocommerce #content input.button:hover, 
		.woocommerce #respond input#submit:hover, 
		.woocommerce button.button:hover, 
		.woocommerce input.button:hover, 
		.woocommerce-page #content input.button:hover, 
		.woocommerce-page #respond input#submit:hover, 
		.woocommerce-page a.button:hover, 
		.woocommerce-page button.button:hover, 
		.woocommerce-page input.button:hover{ background: '.$theme_color.'}

	');
	}

	// Custom css from theme options
	$css = get_custom_option('custom_css');
	if (!empty($css)) {
		addThemeCustomStyle($css);
	}
	
	return getThemeCustomStyles();
};
?>