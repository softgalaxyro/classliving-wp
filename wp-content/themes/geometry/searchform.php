<?php
/*
 * The template for displaying search forms
 * @package Geometry
 */
?>
	<form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<div class="searchFormWrap">
			<div class="searchSubmit"><input type="submit" id="searchsubmit" value="<?php _e( 'Search', 'themerex' ); ?>" /></div>
			<div class="searchField"><input class="" type="search" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php _e( 'Search', 'themerex' ); ?>" /></div>
		</div>
            
            <input type="hidden" name="cat" value="1" />
	</form>
